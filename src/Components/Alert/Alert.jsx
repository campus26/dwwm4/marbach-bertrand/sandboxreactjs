
import "./Alert.css"

export function Alert ({message, alertType}) {
    const isErrorMessage = alertType.toLowerCase() === "error";
    return (
        <>
           <div className={isErrorMessage ? "error bold alert" : "success alert"}>
            {message}
           </div>
        </>
    )
}