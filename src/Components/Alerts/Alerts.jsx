
// import { Button } from './Components/Buttons.jsx'

import { Alert } from "../Alert/Alert";

export function Alerts({data}) {
console.log(data);

  return (
    <>
    {data.map(function(alert){     //fonction anonyme
        return (
                <Alert message={alert.message} alertType={alert.alertType}/>
        )
    })}
    </>
  )
}