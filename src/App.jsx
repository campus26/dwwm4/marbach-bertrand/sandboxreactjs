import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Card } from './Components/Card/Card'
import { Hello } from './Components/Hello/Hello'
import { Alert } from './Components/Alert/Alert'
import { Alerts } from './Components/Alerts/Alerts'
import { Button } from './Components/Button/Button'

function App() {
  const [count, setCount] = useState(0)
 const alerts=[
  {
    "message":"niveau CO supérieur à 10ppm", "alertType":"error"
  },
  {
    "message":"niveau de CO non mesurable", "alertType":"success"
  }
 ]
  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="flex">
        <Card title ="Titre A" image="https://picsum.photos/id/530/200/300" />
        <Card title ="Titre B" image="https://picsum.photos/id/696/200/300" />
        <Card title ="Titre C" image="https://picsum.photos/id/670/200/300" />
      </div>

      <div className="toDoOuterSandBox">
        <div className="toDoInnerFrame">
          <div className="toDoTitle">
            <div className="toDoTitleLeft">
            <h2> Todo</h2>
            </div>
            <div className="toDoTitleRight">
            <h4> Today</h4>
            </div>
            <div className="toDoInputTextBox">
              <input type="text" />
               <button>Valider</button>
              <div className="toDoTasks">
                <div className="toDoTaskStatus">

                </div>
                <div className="toDoTaskText">
                  <p>Buy water</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Alerts data={alerts}/>
      <Button/>

      <div>
      <Hello parametre=""/>
      <Hello parametre="red"/>
      <Hello parametre="World J'affiche autre chose"/>

        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App


